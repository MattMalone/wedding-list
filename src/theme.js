export default {
  breakpoints: ['40em', '52em', '64em'],
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],
  colors: {
    base: '#F3E9DC',
    baseLight: '#FFF6E9',
    baseExtraLight: '#FFFFF6',
    primary: '#F7C59F',
    secondary: '#9E7682',
    tertiary: '#605770',
    highlight: '#4D4861',
    text: '#333',
    disabled: '#F0F0F0',
  },
  spacers: {
    1: '15px',
    2: '30px',
  },
  fonts: {
    body: 'system-ui, sans-serif',
    heading: 'inherit',
    monospace: 'Menlo, monospace',
  },
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.25,
  },
  shadows: {
    small: '0 0 4px rgba(0, 0, 0, .03)',
    large: '0 0 24px rgba(0, 0, 0, .125)',
  },
  variants: {
    card: {
      p: 2,
      bg: '#FFF',
      borderWidth: 1,
      borderColor: 'base',
      borderStyle: 'solid',
      borderRadius: 2,
      boxShadow: 'small',
    },
  },
  text: {},
  buttons: {
    primary: {
      color: 'white',
      bg: 'primary',
      fontWeight: 'bold',
      cursor: 'pointer',
      '&:hover': {
        bg: 'secondary',
      },
      '&[disabled]': {
        bg: 'disabled',
        cursor: 'not-allowed',
      },
    },
    secondary: {
      color: 'white',
      bg: 'secondary',
      fontWeight: 'bold',
      cursor: 'pointer',
      '&:hover': {
        bg: 'tertiary',
      },
      '&[disabled]': {
        bg: 'disabled',
        cursor: 'not-allowed',
      },
    },
    tertiary: {
      color: 'white',
      bg: 'tertiary',
      fontWeight: 'bold',
      cursor: 'pointer',
      '&:hover': {
        bg: 'secondary',
      },
      '&[disabled]': {
        bg: 'disabled',
        cursor: 'not-allowed',
      },
    },
    danger: {
      color: 'white',
      bg: 'highlight',
      fontWeight: 'bold',
      cursor: 'pointer',
      '&[disabled]': {
        bg: 'disabled',
        cursor: 'not-allowed',
      },
    },
  },
}
