import App from './App'
import React from 'react'
import { StaticRouter } from 'react-router-dom'
import express from 'express'
import { renderToString } from 'react-dom/server'
import jwt from 'express-jwt'
import jwks from 'jwks-rsa'
import firebaseAdmin from './firebaseAdmin'
import fetch from 'node-fetch'

import config from './auth_config.json'

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)

const server = express()

const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${config.domain}/.well-known/jwks.json`,
  }),
  audience: 'http://localhost:3000/firebase',
  issuer: `https://${config.domain}/`,
  algorithm: 'RS256',
})

server.get('/firebase', jwtCheck, async (req, res) => {
  const { sub: uid } = req.user

  try {
    const firebaseToken = await firebaseAdmin.auth().createCustomToken(uid)
    res.json({ firebaseToken })
  } catch (err) {
    res.status(500).send({
      message: 'Something went wrong acquiring a Firebase token.',
      error: err,
    })
  }
})

server.get('/rate', async (req, res) => {
  const rate = await fetch(
    'https://api.exchangeratesapi.io/latest?base=CHF&symbols=GBP'
  )
  const data = await rate.json()

  res.status(200).json(data)
})

server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const context = {}
    const markup = renderToString(
      <StaticRouter context={context} location={req.url}>
        <App />
      </StaticRouter>
    )

    if (context.url) {
      res.redirect(context.url)
    } else {
      res.status(200).send(
        `<!doctype html>
    <html lang="">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta charset="utf-8" />
        <title>Welcome to Razzle</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${
          assets.client.css
            ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ''
        }
        ${
          process.env.NODE_ENV === 'production'
            ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
    </head>
    <body>
        <div id="root">${markup}</div>
        <div id="modal-root"></div>
    </body>
</html>`
      )
    }
  })

export default server
