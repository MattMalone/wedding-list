import React, { useContext } from 'react'
import { Route, Switch } from 'react-router-dom'
import { ThemeProvider } from 'emotion-theming'
import { ThemeProvider as StyledThemeProvider } from 'styled-components'
import theme from './theme'
import firebase from 'firebase/app'
import { Box } from 'rebass'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'
import 'firebase/firestore'

import Home from './pages/Home'
import './App.css'
import { Auth0Provider } from './react-auth0-spa'
import config from './auth_config.json'
import { NavBar } from './components/NavBar'
import Login from './pages/Login'
import { AuthenticatedRoute } from './components/AuthenticatedRoute'
import CreateList from './pages/CreateList'
import EditList from './pages/EditList'
import { FirestoreProvider } from './hooks/useFirestore'
import { Container, Main, Footer, Nav } from './App.layout'
import Categories from './pages/Categories'
import Locales from './pages/Locales'
import AddEditItem from './pages/AddEditItem'
import { AppDataSwitch } from './hooks/useAppData'
import CreateCategory from './pages/CreateCategory'

const FirebaseContext = React.createContext()
export const useFirebase = () => useContext(FirebaseContext)

let firebaseInstance = null

const db = (function () {
  if (!firebaseInstance) {
    firebaseInstance = firebase.initializeApp({
      apiKey: 'AIzaSyAcjAZ24W-zWhhSokwfUNr2F__INQpB8AA',
      authDomain: 'wedding-list-da55b.firebaseapp.com',
      databaseURL: 'https://wedding-list-da55b.firebaseio.com',
      projectId: 'wedding-list-da55b',
      storageBucket: 'wedding-list-da55b.appspot.com',
      messagingSenderId: '883970461400',
      appId: '1:883970461400:web:7175fb29595af913d211ad',
    })
  }
  return firebaseInstance
})()

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <StyledThemeProvider theme={theme}>
        <Auth0Provider
          domain={config.domain}
          client_id={config.clientId}
          redirect_uri="http://localhost:3000"
          audience="http://localhost:3000/firebase"
        >
          <FirebaseContext.Provider value={db}>
            <FirestoreProvider>
              <AppDataSwitch>
                <Container>
                  <Nav>
                    <NavBar />
                  </Nav>
                  <Main>
                    <Box
                      flexWrap="wrap"
                      maxWidth={970}
                      margin="auto"
                      minHeight="calc(100vh - 54px)"
                    >
                      <Switch>
                        <Route exact path="/login" component={Login} />

                        <AuthenticatedRoute exact path="/" component={Home} />
                        <AuthenticatedRoute
                          exact
                          path="/list/new"
                          component={CreateList}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/categories"
                          component={Categories}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/categories/add"
                          component={CreateCategory}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/locales"
                          component={Locales}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/list/:listId/edit"
                          component={EditList}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/list/:listId/add-item"
                          component={AddEditItem}
                        />
                        <AuthenticatedRoute
                          exact
                          path="/list/:listId/edit-item/:itemId"
                          component={AddEditItem}
                        />
                      </Switch>
                    </Box>
                  </Main>
                </Container>
              </AppDataSwitch>
            </FirestoreProvider>
          </FirebaseContext.Provider>
        </Auth0Provider>
      </StyledThemeProvider>
    </ThemeProvider>
  )
}

export default App
