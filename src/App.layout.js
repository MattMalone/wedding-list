import styled from 'styled-components'

const Container = styled.div`
  display: grid;

  grid-template-areas:
    'header header'
    'nav content'
    'footer footer';

  grid-template-columns: 250px 1fr;
  grid-template-rows: auto 1fr;
  grid-gap: 0;

  min-height: 100vh;

  font-family: ${({ theme }) => theme.fonts.body};
  line-height: ${({ theme }) => theme.lineHeights.body};
  color: ${({ theme }) => theme.colors.text};

  &,
  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  input,
  textarea,
  p,
  span {
    font-family: ${({ theme }) => theme.fonts.body};
  }

  input:focus,
  textarea:focus {
    border-color: ${({ theme }) => theme.colors.primary};
    outline: none;
  }

  input:disabled {
    cursor: not-allowed;
    background: ${({ theme }) => theme.colors.disabled};
  }
`

const Header = styled.header`
  grid-area: header;
`

const Main = styled.main`
  grid-area: content;
  background: #f9f9fa;
`

const Nav = styled.nav`
  padding: 15px 0;
  grid-area: nav;
  margin-top: -10px;
  background: ${({ theme }) => theme.colors.base};
  border-right: 5px solid ${({ theme }) => theme.colors.secondary};
`

const Footer = styled.footer`
  grid-area: footer;
`

export { Container, Header, Main, Nav, Footer }
