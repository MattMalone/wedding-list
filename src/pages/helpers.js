export const translationsToInitialState = (translations) => {
  return (translations || []).reduce((initialState, translation) => {
    const { localeId, ...fields } = translation

    return {
      ...initialState,
      ...Object.keys(fields).reduce((translationFields, fieldName) => {
        return {
          ...translationFields,
          [`${fieldName}:${localeId}`]: fields[fieldName],
        }
      }, {}),
    }
  }, {})
}

export const deleteList = async (userId, reference, firestore, firebase) => {
  const list = firestore.doc(`users/${userId}/lists/${reference}`)
  const items = await firestore
    .collection(`users/${userId}/lists/${reference}/items`)
    .get()

  await firestore.runTransaction(async (batch) => {
    items.docs.forEach(async (item) => {
      const imgRef = item.data().imageRef

      if (imgRef) {
        firebase.storage().ref(imgRef).delete()
      }

      await batch.delete(item.ref)
    })

    await batch.delete(list)
  })
}
