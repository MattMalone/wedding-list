import React, { useState, useEffect } from 'react'
import { Input, Textarea } from '@rebass/forms'
import { useFireStoreContext } from '../hooks/useFirestore'
import { Button, Box, Card, Heading, Flex } from 'rebass'
import { useHistory, matchPath } from 'react-router-dom'
import { useCollection } from 'react-firebase-hooks/firestore'
import Loader from 'react-loader-spinner'
import { Table, Th, Td } from '../components/Table.tsx'
import theme from '../theme'
import styled from 'styled-components'
import { useFirebase } from '../App'
import { MultiLocaleForm, LocaleButton } from '../components/MultiLocaleForm'
import { useAppData } from '../hooks/useAppData'
import { translationsToInitialState, deleteList } from './helpers'

export const Divider = (props) => (
  <Box
    {...props}
    as="hr"
    my={30}
    mx={-30}
    sx={{
      bg: 'base',
      border: 0,
      height: 1,
    }}
  />
)

export const CardHeader = styled.div`
  padding: ${({ theme }) => theme.spacers[2]};
  margin: -${({ theme }) => theme.spacers[2]};
  background: ${({ theme }) => theme.colors.base};

  p {
    margin: 0;
  }
`

const deleteStep1 = 'Delete list'
const deleteStep2 = 'Yes, I really want to delete this list forever'

const CreateList = () => {
  const { firestore } = useFireStoreContext()
  const firebase = useFirebase()
  const {
    user: { sub: userId },
    locales,
    lists,
  } = useAppData()
  const history = useHistory()
  const match = matchPath(history.location.pathname, {
    path: '/list/:listId/edit',
  })
  const [deleteText, setDeleteText] = useState(deleteStep1)

  const listId = match.params.listId
  const listDetails = lists.find((list) => list.id === listId)

  const [currentLocale, setCurrentLocale] = useState(locales[0].id)

  const [listItems, listItemsLoading] = useCollection(
    firestore
      .collection('users')
      .doc(userId)
      .collection('lists')
      .doc(listId)
      .collection('items')
  )

  const onSubmit = async (values) => {
    const newTranslations = Object.keys(values).map((localeId) => ({
      ...values[localeId],
      localeId,
    }))

    await firestore
      .collection('users')
      .doc(userId)
      .collection('lists')
      .doc(listId)
      .update({ translations: newTranslations })
  }

  if (listItemsLoading) {
    return (
      <Flex flex="1" justifyContent="center" alignItems="center" margin>
        <Loader type="Rings" height={50} width={50} color="#000" />
      </Flex>
    )
  }

  return (
    <>
      <Card p={30} mb={15} mt={15} mx="auto" width="100%">
        <CardHeader>
          <Heading>Edit List</Heading>
        </CardHeader>
        <Divider />

        <MultiLocaleForm
          initialValues={translationsToInitialState(listDetails.translations)}
          onSubmit={(values) => onSubmit(values)}
          fields={[
            { component: Input, name: 'title', label: 'List Title' },
            {
              component: Textarea,
              name: 'description',
              label: 'List Description',
              props: { style: { resize: 'none', height: 100 } },
            },
          ]}
        />
      </Card>

      <Card p={30} mb="auto" mt={15} mx="auto" width="100%">
        <CardHeader>
          <Heading>Items</Heading>
        </CardHeader>
        <Divider />

        {listItems && listItems.docs.length === 0 && (
          <p>This list has no items. Why not add some?</p>
        )}

        {listItems && listItems.docs.length > 0 && (
          <Box mb={theme.spacers[1]}>
            <Box my={theme.spacers[1]}>
              {locales.map((locale, index) => (
                <LocaleButton
                  locale={locale}
                  index={index}
                  onSelect={setCurrentLocale}
                  selectedLocaleId={currentLocale}
                />
              ))}
            </Box>
            <Table style={{ width: '100%' }} cellSpacing="0">
              <thead>
                <tr>
                  <Th>Name</Th>
                  <Th>
                    Price (
                    {currentLocale &&
                      locales.find((l) => l.id === currentLocale).currency}
                    )
                  </Th>
                  <Th>Description</Th>
                  <Th>Image</Th>
                  <Th>Actions</Th>
                </tr>
              </thead>
              <tbody>
                {listItems.docs.map((doc) => {
                  const data = doc.data()
                  const translations = data.translations
                  const translation =
                    (currentLocale
                      ? translations.find((t) => t.localeId === currentLocale)
                      : {}) || {}

                  return (
                    <tr key={doc.id}>
                      <Td>{translation.name || <i>No translation</i>}</Td>
                      <Td>{translation.price || <i>-</i>}</Td>
                      <Td>
                        {translation.description || <i>No translation</i>}
                      </Td>
                      <Td>
                        {data.image ? (
                          <img
                            style={{ maxWidth: '200px' }}
                            src={data.image}
                            alt={translation.name}
                          />
                        ) : (
                          '--'
                        )}
                      </Td>
                      <Td>
                        <Button
                          variant="tertiary"
                          onClick={() => {
                            firestore.doc(doc.ref.path).delete()

                            if (data.imageRef) {
                              firebase.storage().ref(data.imageRef).delete()
                            }
                          }}
                        >
                          Delete
                        </Button>

                        <Button
                          variant="secondary"
                          onClick={() => {
                            history.push(`/list/${listId}/edit-item/${doc.id}`)
                          }}
                        >
                          Edit
                        </Button>
                      </Td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Box>
        )}

        <Button onClick={() => history.push(`/list/${listId}/add-item`)}>
          Add Item
        </Button>
      </Card>

      <Card p={30} mb={15} mt={15} mx="auto" width="100%">
        <CardHeader>
          <Heading>Danger Zone</Heading>
        </CardHeader>
        <Divider />
        <p>Delete your list for good. Note - there is no going back!</p>
        <Button
          variant="danger"
          onClick={() => {
            if (deleteText === deleteStep1) {
              setDeleteText(deleteStep2)
              setTimeout(() => setDeleteText(deleteStep1), 5000)
            } else {
              deleteList(userId, listId, firestore, firebase)
              history.push('/')
            }
          }}
        >
          {deleteText}
        </Button>
      </Card>
    </>
  )
}

export default CreateList
