import React, { useEffect, useMemo } from 'react'
import { Button, Card, Heading, Box, Flex } from 'rebass'
import { useFireStoreContext } from '../hooks/useFirestore'
import { useHistory, Link } from 'react-router-dom'
import moment from 'moment'
import { Table, Th, Td } from '../components/Table.tsx'
import theme from '../theme'
import { useAppData } from '../hooks/useAppData'

const Home = () => {
  const router = useHistory()
  const { firestore } = useFireStoreContext()
  const { locales, user, lists } = useAppData()

  const userId = user.sub

  const primaryLocale = useMemo(() => {
    return locales && locales.find((locale) => locale.primary)
  }, [locales])

  const hasNoLists = useMemo(() => {
    if (lists) {
      return lists.length === 0
    } else {
      return true
    }
  }, [lists])

  // TODO - Move this to onboarding flow
  useEffect(() => {
    if (!user) {
      firestore.collection('users').doc(userId).set(user)
    }
  }, [user])

  const orderedLists = useMemo(() => {
    return lists.sort(
      (a, b) => moment(a.created_at).valueOf() - moment(b.created_at).valueOf()
    )
  }, [lists])

  return (
    <>
      <Box>
        <Box>
          <Heading>
            <p>Welcome back, {user.email}</p>
          </Heading>
        </Box>

        <Box mb={theme.spacers[1]}>
          {!hasNoLists && (
            <Table cellSpacing="0" style={{ textAlign: 'center' }}>
              <thead>
                <tr>
                  <Th>Name</Th>
                  <Th>Description</Th>
                  <Th>Created at</Th>
                  <Th>Actions</Th>
                </tr>
              </thead>
              <tbody>
                {orderedLists.map((list) => {
                  const translation = list.translations.find(
                    (t) => t.localeId === primaryLocale.id
                  )

                  return (
                    <tr key={list.id}>
                      <Td>{translation.title || <i>No translation</i>}</Td>
                      <Td>
                        {translation.description || <i>No translation</i>}
                      </Td>
                      <Td>
                        {moment(
                          list.created_at || <i>No translation</i>
                        ).fromNow()}
                      </Td>
                      <Td>
                        <Button
                          variant="secondary"
                          onClick={() => router.push(`/list/${list.id}/edit`)}
                        >
                          Edit
                        </Button>
                      </Td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          )}
        </Box>

        <Button onClick={() => router.push('/list/new')}>Add new list</Button>
      </Box>
    </>
  )
}

export default Home
