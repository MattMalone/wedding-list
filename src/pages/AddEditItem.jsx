import React, { useState, useMemo } from 'react'
import { Input, Textarea } from '@rebass/forms'
import { useAuth0 } from '../react-auth0-spa'
import { useFireStoreContext } from '../hooks/useFirestore'
import { Box, Card, Heading } from 'rebass'
import { v4 as uuidv4 } from 'uuid'
import { useHistory, matchPath } from 'react-router-dom'
import { MultiLocaleForm } from '../components/MultiLocaleForm'
import { useFirebase } from '../App'
import { Divider } from './EditList'
import { FileInput } from '../components/FileInput'
import { CheckboxGroup } from '../components/CheckboxGroup'
import { useAppData } from '../hooks/useAppData'
import { translationsToInitialState } from './helpers'
import { useDocumentDataOnce } from 'react-firebase-hooks/firestore'

const AddEditItem = () => {
  const { firestore } = useFireStoreContext()
  const auth0 = useAuth0()
  const { user } = auth0 || {}
  const history = useHistory()
  const firebase = useFirebase()
  const { categories, locales } = useAppData()
  const [files, setFiles] = useState([])
  const match = matchPath(history.location.pathname, {
    path: ['/list/:listId/add-item', '/list/:listId/edit-item/:itemId'],
  })

  const listId = match.params.listId
  const itemId = match.params.itemId
  const userId = user.sub
  const isEdit = !!itemId

  const [item, itemLoading] = useDocumentDataOnce(
    firestore.doc(`users/${userId}/lists/${listId}/items/${itemId}`)
  )

  const initialState = useMemo(() => {
    if (itemLoading || !item) return false

    const primaryLocale = locales.find((l) => l.primary)
    const translations = item.translations

    return {
      ...translationsToInitialState(translations),
      [`categories:${primaryLocale.id}`]: item.categories || [],
    }
  }, [item, itemLoading])

  const onSubmit = async (values, singularValues) => {
    const storage = firebase.storage()
    const storageRef = storage.ref()
    const file = files.length > 0 ? files[0] : null
    let imageUrl = item ? item.image : null
    let imageReference = item ? item.imageRef : null

    // Delete the old image first
    if (file && imageReference) {
      try {
        await storage.ref(imageReference).delete()
      } catch (e) {
        // Maybe it's already gone?
      }
    }

    // Upload the new image
    if (file) {
      const ext = file.name.split('.').pop()
      const filename = `public/${file.name}-${+new Date()}.${ext}`
      const newFile = storageRef.child(filename)
      await newFile.put(file, {
        cacheControl: 'max-age=2628000, public',
      })

      imageReference = filename
      imageUrl = await newFile.getDownloadURL()
    }

    await firestore
      .collection('users')
      .doc(userId)
      .collection('lists')
      .doc(listId)
      .collection('items')
      .doc(isEdit ? itemId : uuidv4())
      .set({
        created_at: item ? item.created_at : new Date().toISOString(),
        translations: Object.keys(values).map((localeId) => ({
          ...values[localeId],
          localeId,
        })),
        ...singularValues,
        image: imageUrl,
        imageRef: imageReference,
      })

    history.push(`/list/${listId}/edit`)
  }

  if (itemLoading) {
    return 'Loading...'
  }

  return (
    <Card p={30} my="auto" mx="auto" width="100%">
      <Box>
        <Heading>{isEdit ? 'Edit Item' : 'Add Item'}</Heading>
      </Box>

      <Divider />

      <MultiLocaleForm
        onSubmit={onSubmit}
        initialValues={initialState}
        fields={[
          { component: Input, name: 'name', label: 'Item name' },
          {
            component: Input,
            name: 'price',
            label: 'Price ({currency})',
            props: { type: 'number', step: '0.01' },
            disableIfNotPrimary: true,
          },
          {
            component: Textarea,
            name: 'description',
            label: 'List Description',
            props: { style: { resize: 'none', height: 100 } },
          },
          {
            component: CheckboxGroup,
            name: 'categories',
            label: 'Item Categories',
            single: true,
            props: {
              options: categories.map(({ id, translations }) => {
                const primaryTranslation = translations.find(
                  (t) => t.localeId === locales.find((l) => l.primary).id
                )

                return {
                  label: primaryTranslation.name,
                  value: id,
                }
              }),
            },
          },
          {
            component: FileInput,
            name: 'image',
            label: 'Image',
            single: true,
            props: {
              accept: 'image/png, image/jpeg',
              onFileChange: setFiles,
              existingValue: item ? item.image : null,
            },
          },
        ]}
      />
    </Card>
  )
}

export default AddEditItem
