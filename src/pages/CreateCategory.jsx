import React, { useMemo } from 'react'
import { Form, Field } from 'react-final-form'
import { Label, Input, Textarea } from '@rebass/forms'
import { useAuth0 } from '../react-auth0-spa'
import { useFireStoreContext } from '../hooks/useFirestore'
import { Button, Box, Card, Heading } from 'rebass'
import { v4 as uuidv4 } from 'uuid'
import { useHistory } from 'react-router-dom'
import { useCollectionData } from 'react-firebase-hooks/firestore'
import { MultiLocaleForm } from '../components/MultiLocaleForm'

const Divider = (props) => (
  <Box
    {...props}
    as="hr"
    my={30}
    mx={-30}
    sx={{
      bg: 'gray',
      border: 0,
      height: 1,
    }}
  />
)

const CreateCategory = () => {
  const { firestore } = useFireStoreContext()
  const auth0 = useAuth0()
  const { user } = auth0 || {}
  const history = useHistory()

  const userId = user.sub

  const newListId = uuidv4()

  const onSubmit = async (values, primaryLocaleId) => {
    // Values are structured data from MultiLocaleForm
    await firestore
      .collection('users')
      .doc(userId)
      .collection('categories')
      .doc(newListId)
      .set({
        created_at: new Date().toISOString(),
        translations: Object.keys(values).map((localeId) => ({
          ...values[localeId],
          localeId,
        })),
      })

    history.push('/categories')
  }

  return (
    <Card p={30} my="auto" mx="auto" width="100%">
      <Box>
        <Heading>Add new Category</Heading>
      </Box>
      <Divider />

      <MultiLocaleForm
        onSubmit={onSubmit}
        fields={[
          { component: Input, name: 'name', label: 'Category Name' },
          {
            component: Textarea,
            name: 'description',
            label: 'Category Description',
            props: { style: { resize: 'none', height: 100 } },
          },
        ]}
      />
    </Card>
  )
}

export default CreateCategory
