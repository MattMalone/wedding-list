import React from 'react'
import { useAuth0 } from '../react-auth0-spa'
import { Button, Card, Heading, Flex } from 'rebass'
import { Redirect } from 'react-router-dom'
import Loader from 'react-loader-spinner'

const Login = ({ firestore }) => {
  const auth0 = useAuth0()
  const {
    loginWithRedirect,
    isAuthenticated,
    loading
  } = auth0 || {}

  if (loading) {
    return (
      <Flex flex="1" justifyContent="center" alignItems="center" margin>
        <Loader type="Rings" height={50} width={50} color="#000" />
      </Flex>
    )
  }

  if (isAuthenticated) {
    return <Redirect to='/' />
  }

  return (
    <Flex flex="1" justifyContent="center" alignItems="center" margin>
      <Card p={15}>
        <Flex alignItems="center" flexDirection="column">
          <Heading mb="15px">You are not logged in.</Heading>
          <Button onClick={() => loginWithRedirect({})}>Log in</Button>
        </Flex>
      </Card>
    </Flex>
  )
}

export default Login
