import React, { useState } from 'react'
import { useFireStoreContext } from '../hooks/useFirestore'
import { Card, Heading, Button, Box } from 'rebass'
import { CardHeader, Divider } from './EditList'
import { Table, Th, Td } from '../components/Table.tsx'
import { useAppData } from '../hooks/useAppData'
import theme from '../theme'
import { LocaleButton } from '../components/MultiLocaleForm'
import { useHistory } from 'react-router-dom'

const Categories = () => {
  const { firestore } = useFireStoreContext()
  const {
    categories,
    locales,
    user: { sub: userId },
  } = useAppData()
  const history = useHistory()

  const [currentLocale, setCurrentLocale] = useState(locales[0].id)

  const handleDeleteCategory = (id) => {
    const path = `users/${userId}/categories`
    firestore.collection(path).doc(id).delete()
  }

  return (
    <>
      <Card p={30} mb={15} mt={15} mx="auto" width="100%">
        <CardHeader>
          <Heading>Categories</Heading>
          <p>Add categories to organise items on your lists</p>
        </CardHeader>
        <Divider />

        <Box my={theme.spacers[1]}>
          {locales.map((locale, index) => (
            <LocaleButton
              locale={locale}
              index={index}
              onSelect={setCurrentLocale}
              selectedLocaleId={currentLocale}
            />
          ))}
        </Box>

        <Table cellSpacing="0">
          <thead>
            <tr>
              <Th>Name</Th>
              <Th>Description</Th>
              <Th></Th>
            </tr>
          </thead>
          <tbody>
            {categories &&
              categories.map((doc) => {
                const translation =
                  doc.translations.find((t) => t.localeId === currentLocale) ||
                  {}

                return (
                  <tr key={doc.id}>
                    <Td>{translation.name}</Td>
                    <Td>{translation.description}</Td>
                    <Td>
                      <Button
                        onClick={() => handleDeleteCategory(doc.id)}
                        variant="tertiary"
                      >
                        Delete
                      </Button>
                    </Td>
                  </tr>
                )
              })}
          </tbody>
        </Table>

        <Button onClick={() => history.push(`/categories/add`)}>
          Add Item
        </Button>
      </Card>
    </>
  )
}

export default Categories
