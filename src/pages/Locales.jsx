import React from 'react'
import { useFireStoreContext } from '../hooks/useFirestore'
import { useAuth0 } from '../react-auth0-spa'
import { CardHeader, Divider } from './EditList'
import { Table, Th, Td } from '../components/Table.tsx'
import { Badge } from '../components/Badge'
import { Select } from '@rebass/forms'
import { Form, Field, FormSpy } from 'react-final-form'
import { v4 as uuidv4 } from 'uuid'

import { continents, countries, languagesAll } from 'countries-list'
import theme from '../theme'
import { useAppData } from '../hooks/useAppData'
import { Box, Button, Heading, Card } from 'rebass'

const europe = Object.keys(continents).find((c) => continents[c] === 'Europe')

const europeanCountries = Object.keys(countries).reduce((list, country) => {
  if (countries[country].continent === europe) {
    return [...list, { ...countries[country], iso: country }]
  }
  return list
}, [])

const Locales = () => {
  const { firestore } = useFireStoreContext()
  const auth0 = useAuth0()
  const { user } = auth0 || {}
  const { locales } = useAppData()

  const userId = user.sub

  const handleDeleteLocale = (id) => {
    const path = `users/${userId}/locales`
    firestore.collection(path).doc(id).delete()
  }

  const handleSubmit = (values) => {
    const path = `users/${userId}/locales`
    firestore.collection(path).doc(uuidv4()).set(values)
  }

  const handleMakePrimary = (id) => {
    const path = `users/${userId}/locales`
    const batch = firestore.batch()

    locales.forEach((doc) => {
      const docRef = firestore.collection(path).doc(doc.id)

      if (doc.id === id) {
        batch.update(docRef, { ...doc, primary: true })
      } else {
        batch.update(docRef, { ...doc, primary: false })
      }
    })

    batch.commit()
  }

  return (
    <>
      <Card p={30} mb={15} mt={15} mx="auto" width="100%">
        <CardHeader>
          <Heading>Locales</Heading>
          <p>
            Add locales so your guests can see the items in their native tongue
          </p>
        </CardHeader>
        <Divider />

        <Form onSubmit={(values) => handleSubmit(values)}>
          {({ handleSubmit, form }) => (
            <Table cellSpacing="0">
              <thead>
                <tr>
                  <Th>Country</Th>
                  <Th>Currency</Th>
                  <Th>Language</Th>
                  <Th></Th>
                </tr>
              </thead>
              <tbody>
                {locales &&
                  locales.map((doc) => (
                    <tr key={doc.id}>
                      <Td>
                        {countries[doc.country].name}{' '}
                        {doc.primary ? <Badge>Primary</Badge> : null}
                      </Td>
                      <Td>{doc.currency}</Td>
                      <Td>{languagesAll[doc.language].name}</Td>
                      <Td>
                        <Box mr={theme.spacers[1]} display="inline">
                          <Button
                            onClick={() => handleMakePrimary(doc.id)}
                            variant="secondary"
                            disabled={doc.primary}
                          >
                            Make Primary
                            {console.log(doc)}
                          </Button>
                        </Box>
                        <Button
                          onClick={() => handleDeleteLocale(doc.id)}
                          variant="tertiary"
                          disabled={doc.primary}
                        >
                          Delete
                        </Button>
                      </Td>
                    </tr>
                  ))}
                <tr>
                  <Td>
                    <Field name="country">
                      {({ input }) => (
                        <Select {...input}>
                          <option disabled value="" selected>
                            Select a country
                          </option>
                          {europeanCountries
                            .sort((a, b) => a.name.localeCompare(b.name))
                            .map((country) => (
                              <option key={country.name} value={country.iso}>
                                {country.name}
                              </option>
                            ))}
                        </Select>
                      )}
                    </Field>
                  </Td>
                  <Td>
                    <FormSpy>
                      {({ values }) => (
                        <Field name="currency">
                          {({ input }) => (
                            <Select {...input} disabled={!values.country}>
                              <option disabled value="" selected>
                                Select a currency
                              </option>
                              {values.country &&
                                countries[values.country].currency
                                  .split(',')
                                  .sort((a, b) => a.localeCompare(b))
                                  .map((currency) => (
                                    <option key={currency} value={currency}>
                                      {currency}
                                    </option>
                                  ))}
                            </Select>
                          )}
                        </Field>
                      )}
                    </FormSpy>
                  </Td>
                  <Td>
                    <FormSpy>
                      {({ values }) => (
                        <Field name="language">
                          {({ input }) => (
                            <Select {...input} disabled={!values.country}>
                              <option disabled value="" selected>
                                Select a language
                              </option>
                              {values.country &&
                                countries[values.country].languages
                                  .sort((a, b) => a && a.localeCompare(b))
                                  .map((language) => (
                                    <option key={language} value={language}>
                                      {languagesAll[language].name}
                                    </option>
                                  ))}
                            </Select>
                          )}
                        </Field>
                      )}
                    </FormSpy>
                  </Td>
                  <Td>
                    <Button
                      onClick={() => {
                        handleSubmit()
                        form.reset()
                      }}
                      variant="secondary"
                      type="submit"
                    >
                      Add Locale
                    </Button>
                  </Td>
                </tr>
              </tbody>
            </Table>
          )}
        </Form>
      </Card>
    </>
  )
}

export default Locales
