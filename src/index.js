import http from 'http'
import fs from 'fs'
import https from 'https'

let app = require('./server').default

const server = http.createServer(app)

let currentApp = app

if (process.env.USE_HTTPS) {
  // Certificate
  const privateKey = fs.readFileSync(
    '/etc/letsencrypt/live/weddinglist.tk/privkey.pem',
    'utf8'
  )
  const certificate = fs.readFileSync(
    '/etc/letsencrypt/live/weddinglist.tk/cert.pem',
    'utf8'
  )
  const ca = fs.readFileSync(
    '/etc/letsencrypt/live/weddinglist.tk/chain.pem',
    'utf8'
  )

  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca,
  }

  const httpsServer = https.createServer(credentials, app)

  httpsServer.listen(443)
}

server
  .listen(process.env.PORT || 3000, () => {
    console.log('🚀 started')
  })
  .on('error', (error) => {
    console.log(error)
  })

if (module.hot) {
  console.log('✅  Server-side HMR Enabled!')

  module.hot.accept('./server', () => {
    console.log('🔁  HMR Reloading `./server`...')

    try {
      app = require('./server').default
      server.removeListener('request', currentApp)
      server.on('request', app)
      currentApp = app
    } catch (error) {
      console.error(error)
    }
  })
}
