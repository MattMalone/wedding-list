import React, { createContext, useContext } from 'react'
import { useState, useEffect } from 'react'
import { useAuth0 } from '../react-auth0-spa'
import { useFirebase } from '../App'
import { Flex } from 'rebass'
import Loader from 'react-loader-spinner'

const getFirebaseToken = async (token) => {
  const response = await fetch('/firebase', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  const data = await response.json()

  return data.firebaseToken
}

export const useFirestore = () => {
  const [loading, setLoading] = useState(true)
  const { getTokenSilently, loading: authLoading } = useAuth0() || {}
  const db = useFirebase()
  const [store, setStore] = useState(null)

  const setLoadingComplete = () => {
    setStore(db.firestore())
    setLoading(false)
  }

  const fetchToken = async () => {
    const token = await getTokenSilently()
    const firebaseToken = await getFirebaseToken(token)

    db.auth()
      .signInWithCustomToken(firebaseToken)
      .then(() => {
        setLoadingComplete()
      })
  }

  useEffect(() => {
    db.auth().onAuthStateChanged((user) => {
      if (user) {
        setLoadingComplete()
      } else {
        fetchToken()
      }
    })
  })

  return {
    store,
    loading: loading || authLoading,
  }
}

export const withFireStore = (Component) => (props) => {
  const { store, loading } = useFirestore()

  if (loading) {
    return (
      <Flex flex="1" justifyContent="center" alignItems="center" margin>
        <Loader type="Rings" height={50} width={50} color="#000" />
      </Flex>
    )
  }

  return <Component {...props} firestore={store} />
}

const FirestoreContext = createContext({
  firestore: null,
})

export const FirestoreProvider = ({ children }) => {
  const { store, loading } = useFirestore()

  if (loading) {
    return (
      <Flex flex="1" justifyContent="center" alignItems="center" margin>
        <Loader type="Rings" height={50} width={50} color="#000" />
      </Flex>
    )
  }

  return (
    <FirestoreContext.Provider value={{ firestore: store }}>
      {children}
    </FirestoreContext.Provider>
  )
}

export const useFireStoreContext = () => useContext(FirestoreContext)
