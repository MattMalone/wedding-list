import { useAuth0 } from '../react-auth0-spa'
import { useFireStoreContext } from './useFirestore'
import React, { createContext, useContext } from 'react'
import {
  useCollectionData,
  useDocumentData,
} from 'react-firebase-hooks/firestore'

const AppDataContext = createContext()

export const AppDataSwitch = ({ children }) => {
  const auth0 = useAuth0()
  const { user } = auth0 || {}

  if (user && user.sub) {
    return <AppDataProvider>{children}</AppDataProvider>
  } else {
    return (
      <AppDataContext.Provider value={{ user: user }}>
        {children}
      </AppDataContext.Provider>
    )
  }
}

export const AppDataProvider = ({ children }) => {
  const { firestore } = useFireStoreContext()
  const auth0 = useAuth0()
  const { user } = auth0 || {}

  const userId = user && user.sub

  const [userRecord, userLoading] = useDocumentData(
    firestore.doc(`users/${userId}`)
  )

  const [locales, localesLoading] = useCollectionData(
    firestore.collection('users').doc(userId).collection('locales'),
    { idField: 'id' }
  )

  const [lists, listsLoading] = useCollectionData(
    firestore.collection('users').doc(userId).collection('lists'),
    { idField: 'id' }
  )

  const [
    categories,
    categoriesLoading,
  ] = useCollectionData(
    firestore.collection('users').doc(userId).collection('categories'),
    { idField: 'id' }
  )

  const contextValue = {
    categories,
    lists,
    locales,
    user: userRecord,
  }

  const isLoading =
    categoriesLoading || listsLoading || localesLoading || userLoading

  if (isLoading) return <span>Loading...</span>

  return (
    <AppDataContext.Provider value={contextValue}>
      {children}
    </AppDataContext.Provider>
  )
}

export const useAppData = () => useContext(AppDataContext)
