import { useState, useEffect } from 'react'

const useExchangeRate = () => {
  const [loading, setLoading] = useState(true)
  const [exchangeRate, setExchangeRate] = useState(null)

  const fetchRate = async () => {
    setLoading(true)
    const rate = await fetch('/rate')
    const json = await rate.json()

    setExchangeRate(json.rates.GBP)
    setLoading(false)
  }

  useEffect(() => {
    fetchRate()
  }, [])

  return {
    loading,
    exchangeRate,
  }
}

export default useExchangeRate
