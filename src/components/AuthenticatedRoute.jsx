import React from 'react'
import { useAuth0 } from '../react-auth0-spa'
import Loader from 'react-loader-spinner'
import { Flex } from 'rebass'
import { Redirect, Route } from 'react-router-dom'

export const AuthenticatedRoute = (props) => {
  const auth0 = useAuth0()
  const {
    isAuthenticated,
    loading
  } = auth0 || {}

  if (loading) {
    return (
      <Flex flex="1" justifyContent="center" alignItems="center" margin>
        <Loader type="Rings" height={50} width={50} color="#000" />
      </Flex>
    )
  }

  if (!isAuthenticated) {
    return <Redirect to='/login' />
  }

  return <Route {...props} />
}
