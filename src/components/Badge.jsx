import styled from 'styled-components'

export const Badge = styled.span`
  margin-left: 5px;
  padding: 3px;
  background: ${({ theme }) => theme.colors.primary};
  font-weight: bold;
  color: #fff;
  font-size: 12px;
  border-radius: 3px;
`
