import styled from 'styled-components'

const Table = styled.table`
  width: 100%;
  border: 1px solid ${({ theme }) => theme.colors.tertiary};
  background: #fff;

  tbody tr:last-of-type td {
    border-bottom: 0;
  }
`

const Th = styled.th`
  background: ${({ theme }) => theme.colors.secondary};
  color: #fff;
  border-bottom: 1px solid ${({ theme }) => theme.colors.tertiary};
  padding: ${({ theme }) => theme.spacers[1]};
`

const Td = styled.td`
  color: ${({ theme }) => theme.colors.text};
  border-bottom: 1px solid ${({ theme }) => theme.colors.tertiary};
  padding: ${({ theme }) => theme.spacers[1]};

  img {
    display: block;
    margin: auto;
  }
`

export { Table, Th, Td }
