import React, { useState, useEffect, useMemo } from 'react'
import { Form, Field } from 'react-final-form'
import { countries, languagesAll } from 'countries-list'
import { Box, Flex, Button } from 'rebass'
import theme from '../theme'
import { Label } from '@rebass/forms'
import { useAppData } from '../hooks/useAppData'

const formatString = (str, locale) => {
  return str.replace(/(\{[a-z]+\})/g, (group) => {
    const key = group.replace(/[{}]/g, '')
    return locale[key]
  })
}

export const LocaleButton = ({ locale, selectedLocaleId, onSelect, index }) => (
  <Button
    key={locale.id}
    variant={locale.id === selectedLocaleId ? 'secondary' : 'tertiary'}
    onClick={() => onSelect(locale.id)}
    ml={index > 0 ? theme.spacers[1] : 0}
  >
    {languagesAll[locale.language].native} {countries[locale.country].emoji}
  </Button>
)

export const MultiLocaleForm = ({ onSubmit, fields, initialValues }) => {
  const { locales } = useAppData()

  const [currentLocale, setCurrentLocale] = useState()

  const primaryLocale = useMemo(() => {
    return locales && locales.find((locale) => locale.primary)
  }, [locales])

  useEffect(() => {
    if (!currentLocale && locales)
      setCurrentLocale(locales.find((l) => !l.primary).id)
  }, [locales])

  const handleSubmit = async (values) => {
    const structuredFields = Object.keys(values).reduce((structured, name) => {
      const newStructured = { ...structured }
      const [fieldName, localeId] = name.split(':')

      // If it's a singular field then don't include it
      if (fields.find((f) => f.name === fieldName).single) return newStructured

      if (!newStructured[localeId]) newStructured[localeId] = {}

      newStructured[localeId][fieldName] = values[name]

      return newStructured
    }, {})

    // Singular fields end up under the primary locale
    const singularFields = fields
      .filter((field) => field.single)
      .map((f) => f.name)
      .reduce((singleFields, fieldName) => {
        return {
          ...singleFields,
          [fieldName]: values[`${fieldName}:${primaryLocale.id}`],
        }
      }, {})

    await onSubmit(structuredFields, singularFields)
  }

  return (
    <Form onSubmit={handleSubmit} initialValues={initialValues}>
      {({ handleSubmit, submitting, pristine }) => (
        <>
          <Box pb={theme.spacers[1]}>
            <Flex justifyContent="flex-end">
              {locales
                .filter((locale) => !locale.primary)
                .map((locale, index) => (
                  <LocaleButton
                    locale={locale}
                    index={index}
                    selectedLocaleId={currentLocale}
                    onSelect={setCurrentLocale}
                  />
                ))}
            </Flex>
          </Box>
          <Flex>
            {locales
              .sort((a, b) => (a.primary > b.primary ? -1 : 1))
              .map((locale, index) => (
                <Box
                  width="50%"
                  pr={index === 0 ? theme.spacers[1] : 0}
                  pl={index > 0 ? theme.spacers[1] : 0}
                  display={
                    locale.id === currentLocale || locale === primaryLocale
                      ? 'block'
                      : 'none'
                  }
                >
                  {fields.map((field) => {
                    const isPrimaryLocale = locale.id === primaryLocale.id
                    const isSingleField = field.single

                    if (isSingleField && !isPrimaryLocale) {
                      return null
                    }

                    return (
                      <Field name={`${field.name}:${locale.id}`}>
                        {({ input }) => (
                          <Box mb={theme.spacers[1]}>
                            <Label>{formatString(field.label, locale)}</Label>
                            <field.component
                              {...input}
                              {...field.props}
                              disabled={
                                field.disableIfNotPrimary && !isPrimaryLocale
                              }
                            />
                          </Box>
                        )}
                      </Field>
                    )
                  })}
                </Box>
              ))}
          </Flex>
          <Button
            type="submit"
            onClick={handleSubmit}
            disabled={submitting || pristine}
          >
            Save
          </Button>
        </>
      )}
    </Form>
  )
}
