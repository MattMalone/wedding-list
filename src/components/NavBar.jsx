import React from 'react'
import { Flex, Box, Text, Button, Heading } from 'rebass'
import { Link } from 'react-router-dom'
import { useAuth0 } from '../react-auth0-spa'
import styled from 'styled-components'
import theme from '../theme'

const NavHeading = styled(Heading)`
  color: ${({ theme }) => theme.colors.highlight};
`

const NavList = styled.ul`
  list-style: none outside none;
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: column;
  height: 100%;
`

const NavLink = styled(Link)`
  padding: ${({ theme }) => theme.spacers[1]};
  display: block;
  text-decoration: none;
  color: ${({ theme }) => theme.colors.text};

  &:hover {
    background: rgba(255, 255, 255, 0.2);
  }
`

export const NavBar = () => {
  const { loading, isAuthenticated, logout } = useAuth0() || {}

  return (
    <NavList>
      <li>
        <NavHeading p={theme.spacers[1]} fontWeight="bold">
          Wedding List
        </NavHeading>
      </li>
      <li>
        <NavLink to="/profile">Profile</NavLink>
      </li>
      <li>
        <NavLink to="/">Lists</NavLink>
      </li>
      <li>
        <NavLink to="/categories">Categories</NavLink>
      </li>
      <li>
        <NavLink to="/locales">Locales</NavLink>
      </li>
      {isAuthenticated && (
        <li style={{ marginTop: 'auto', padding: '10px' }}>
          <Button onClick={logout}>Logout</Button>
        </li>
      )}
    </NavList>
  )
}
