import React, { useState, useEffect } from 'react'
import { Label } from '@rebass/forms'

export const CheckboxGroup = ({ name, options, value, onChange }) => {
  const [selected, setSelected] = useState(value || [])

  // Update if value changes
  useEffect(() => {
    setSelected(value)
  }, [value])

  const handleChange = (e) => {
    const { checked, value } = e.target

    if (checked && !selected.includes(value)) {
      setSelected([...selected, value])
    } else if (!checked && selected.includes(value)) {
      setSelected(selected.filter((v) => v !== value))
    }
  }

  // Trigger onChange after setState
  useEffect(() => {
    onChange(selected)
  }, [selected])

  return (
    <fieldset>
      {options.map((option) => (
        <Label>
          <input
            type="checkbox"
            name={`${name}[]`}
            onChange={handleChange}
            value={option.value}
            checked={selected.includes(option.value)}
          />
          {option.label}
        </Label>
      ))}
    </fieldset>
  )
}
