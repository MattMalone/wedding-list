import React from 'react'
import { Input } from '@rebass/forms'

export const FileInput = ({
  onChange,
  onFileChange,
  existingValue,
  ...props
}) => {
  const handleChange = (e) => {
    console.log(e.target.files)
    onChange(e)
    onFileChange && onFileChange(e.target.files)
  }

  return (
    <>
      {existingValue && (
        <img src={existingValue} style={{ maxWidth: '100%' }} />
      )}
      <Input type="file" {...props} onChange={handleChange} />
    </>
  )
}
