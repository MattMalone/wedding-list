import firebaseAdmin from 'firebase-admin'

const serviceAccount = require('../secrets/firebase');

let app = null

export default (function () {
  if (!app) {
    app = firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(serviceAccount),
      databaseURL: `https://${serviceAccount.project_id}.firebaseio.com`
    });
  }

  return app
}())